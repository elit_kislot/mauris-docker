#!/bin/bash

cd /var/www
rm -rf ./rest
git clone https://gitlab+deploy-token-23654:sjbGQiAQyhx_2MK9tCPw@gitlab.com/elit_kislot/mauris-rest.git rest
cd /var/www/rest
composer install --no-interaction
chmod 777 var/ -R
bin/console doctrine:database:create
bin/console doctrine:schema:update --force
bin/console doctrine:fixtures:load
bin/console ca:cl --env=prod
bin/console ca:war --env=prod
chmod 777 var/ -R
bin/console assets:install --symlink

php-fpm7.2 -F -R