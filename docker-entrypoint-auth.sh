#!/bin/bash

cd /var/www
rm -rf ./auth
git clone https://gitlab+deploy-token-23433:xF_fxN68nao35tsFhzPx@gitlab.com/elit_kislot/mauris-auth.git ./auth
cd /var/www/auth
composer install --no-interaction
chmod 777 var/ -R
bin/console doctrine:database:create
bin/console doctrine:schema:update --force
bin/console doctrine:fixtures:load
bin/console ca:cl --env=prod
bin/console ca:war --env=prod
chmod 777 var/ -R

#create test user
bin/console auth:user:create test test

php-fpm7.2 -F -R


